package ru.belonogov.server;

import java.io.IOException;



import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;


public class Server {
    private final int serverPort;
    private ServerSocket server;


    public Server(int port) throws SQLException {
        serverPort = port;

        Socket socket = null;
        try {
            server = new ServerSocket(serverPort);
            System.out.println("Echo Server created. Waiting for a client...");
            while(true) {
                socket = server.accept();
                System.out.println("Client connected.");
                new Thread(new ClientHandler(socket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                server.close();
                System.out.println("Server closed.");
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}