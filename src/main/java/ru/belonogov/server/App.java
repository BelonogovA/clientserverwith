package ru.belonogov.server;

import java.sql.SQLException;

public class App {
    public static final int PORT = 8181;

    public static void main(String[] args) throws SQLException {
        new Server(PORT);
    }
}
