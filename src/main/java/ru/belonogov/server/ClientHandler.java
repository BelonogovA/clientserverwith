package ru.belonogov.server;

import com.mysql.cj.mysqlx.protobuf.MysqlxCrud;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.sql.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.io.File;

public class ClientHandler implements Runnable {
    private Socket socket;
    private PrintWriter out;
    private Scanner in;
    ArrayList<String> words = new ArrayList<String>();
    ArrayList<String> answersBlock = new ArrayList<String>();
    String login;
    String password;
    String regOrLog;

    public ClientHandler(Socket s) throws IOException, SQLException {
        try {
            this.socket = s;
            out = new PrintWriter(s.getOutputStream());
            in = new Scanner(s.getInputStream());
        } catch (IOException e) {
        }
    }

    @Override
    public void run() {
        try {
            Connection connection = getConnection();
            tryAuthorization(connection);
            recordClientMessage();
            choiceOfCommand(connection);

        } catch (SQLException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void tryAuthorization(Connection connection) throws SQLException {
        out.write("Доброго времени суток!\n Если у вас уже есть логин и пароль, введите 1, для регистрации нажмите 2\n");
        out.flush();
        regOrLog = in.nextLine();

        out.write("Введите логин\n");
        out.flush();
        login = in.nextLine();

        out.write("Введите пароль\n");
        out.flush();
        password = in.nextLine();
        if (Objects.equals(regOrLog, "1")) {
            PreparedStatement statement = connection.prepareStatement("SELECT role FROM studnprep WHERE name=? AND pass=?;");
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet role = statement.executeQuery();

            if (role.next()) {
                System.out.println("Connected");
                out.write("Тебе повезло, логин и пасс подошли\n");
                out.flush();
                int hisRole = role.getInt("role");
                if (hisRole == 1) {
                    out.write("\n" + "Привет " + login + "\n");
                    out.flush();
                    listStudent();
                }
                if (hisRole == 2) {
                    out.write("\n" + "Привет " + login + "\n");
                    out.write("\n");
                    out.flush();
                    listPrepod();
                }
            } else {
                System.out.println("Error, wrong login/password ");
                out.write("\nError\n Try again...\n");
                out.flush();
                tryAuthorization(connection);
            }
        }
        if (Objects.equals(regOrLog, "2")) {
            out.write("Если вы студент, нажмите 1, если преподаватель нажмите 2\n");
            out.flush();
            int regRole = in.nextInt();
            PreparedStatement registr = connection.prepareStatement("INSERT INTO studnprep (name, pass, role) VALUES (?,?,?)");
            registr.setString(1, login);
            registr.setString(2, password);
            registr.setInt(3, regRole);
            registr.executeUpdate();
            out.write("Регистрация прошла успешно!\n");
            out.flush();
            if (regRole == 1) {
                listStudent();
            } else {
                listPrepod();
            }
        }
    }

    private void listStudent() {
        out.write("\n'read' - Читать лекцию\n" +
                "'testing' - Пройти тестирование\n" +
                "'chat' - Зайти в чат\n\n");
        out.flush();
    }

    private void listPrepod() {
        out.write("\n'create' - Создать тест\n" +
                "'check' - Проверить тест\n" +
                "'upload' - Загрузить лекцию\n" +
                "'chat' - Зайти в чат\n\n");
        out.flush();
    }

    private void recordClientMessage() {
        words.clear();
        if (in.hasNext()) { //считывает строку
            String message = in.nextLine(); //записываем ее в переменную message;
            StringTokenizer stringTokenizer = new StringTokenizer(message); //Берем токенайзер и принимаем в него message
            while (stringTokenizer.hasMoreTokens()) {
                words.add(stringTokenizer.nextToken());
            }
        }
    }

    private void readLecture(Connection connection, String firstWord) throws SQLException, IOException {
        if (firstWord.equals("read")) {
            String list[] = new File("C:\\Users\\Bel\\Desktop\\ClientServerWithMysqlAndTxt\\src\\main\\resources\\Lecture\\").list();
            for (int i = 0; i < list.length; i++)
                out.write(list[i] + "\n");
            out.flush();
            if (in.hasNext()) {
                String message = in.nextLine();
                Process process = new ProcessBuilder().command("notepad", "C:\\Users\\Bel\\Desktop\\ClientServerWithMysqlAndTxt\\src\\main\\resources\\Lecture\\" + message).start();
            }
        }
    }

    private void testing(Connection connection, String firstWord) throws SQLException, IOException {
        if (firstWord.equals("testing")) {

            String list[] = new File("C:\\Users\\Bel\\Desktop\\ClientServerWithMysqlAndTxt\\src\\main\\resources\\tests").list();
            for (int i = 0; i < list.length; i++)
                out.write(list[i] + "\n");
            out.flush();
            out.write("\nВведите название теста который хотите пройти\n");
            out.flush();
            if (in.hasNext()) {
                String message = in.nextLine();

                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream
                        ("C:/Users/Bel/Desktop/ClientServerWithMysqlAndTxt/src/main/resources/tests/" + message), "windows-1251"));
                String strLine;
                out.write("\nВводите ответы цифрами   ");

                //create student строчук для ответов и внести фамилию

                PreparedStatement createStud = connection.prepareStatement(
                        "INSERT INTO answers(Student,`1`,`2`,`3`,`4`,`5`,`6`,`7`,`8`,`9`,`10`,`testName`) " +
                                "VALUES (?,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,? ) ;");
                createStud.setString(1, login);
                createStud.setString(2, message);
                createStud.executeUpdate();

                while ((strLine = br.readLine()) != null) {
                    int count = 0;
                    out.write(strLine + "\n");
                    out.flush();
                    in.hasNext();
                    String answer = in.nextLine();
                    out.write(answer + "\n");
                    out.flush();
                    answersBlock.add(answer);
                }
                PreparedStatement sendAnswers = connection.prepareStatement("UPDATE answers " +
                        "SET `1`=?,`2`=?,`3`=?,`4`=?,`5`=?,`6`=?,`7`=?,`8`=?,`9`=?,`10`=?" +
                        " WHERE Student=?");
                sendAnswers.setString(1, answersBlock.get(0));
                sendAnswers.setString(2, answersBlock.get(1));
                sendAnswers.setString(3, answersBlock.get(2));
                sendAnswers.setString(4, answersBlock.get(3));
                sendAnswers.setString(5, answersBlock.get(4));
                sendAnswers.setString(6, answersBlock.get(5));
                sendAnswers.setString(7, answersBlock.get(6));
                sendAnswers.setString(8, answersBlock.get(7));
                sendAnswers.setString(9, answersBlock.get(8));
                sendAnswers.setString(10, answersBlock.get(9));
                sendAnswers.setString(11, login);
                sendAnswers.executeUpdate();
                out.write("Thank you man");
                makeOtherCommand(connection);
            }
        }
    }

    private void createTest(Connection connection, String firstWord) throws IOException, SQLException {
        if (firstWord.equals("create")) {

            out.write("Для создания, введите новое название для теста\n\n");
            out.write("Напоминаю, чтобы тест корректно отображался, один вопрос и ответы к нему должны быть в одной строке, второй вопрос и ответы к нему, во второй и т.д.\n\n");
            out.flush();
            if (in.hasNext()) {
                String message = in.nextLine();
                Process process = new ProcessBuilder().command("notepad", "C:\\Users\\Bel\\Desktop\\ClientServerWithMysqlAndTxt\\src\\main\\resources\\Lecture\\" + message).start();
            }
        }
    }

    private void checkTest(Connection connection, String firstWord) throws SQLException {
        if (firstWord.equals("check")) {

            out.write("Выберите студента, ответы которого хотите проверить\n");
            out.flush();
            PreparedStatement check = connection.prepareStatement("SELECT Student FROM answers");
            ResultSet allStudents = check.executeQuery();
            while (allStudents.next()) {
                String student = allStudents.getString("Student");
                out.write(student + "\n");
                out.flush();
            }
            out.write("\n");
            out.flush();

            if (in.hasNext()) {
                String message = in.nextLine();
                PreparedStatement choice = connection.prepareStatement("SELECT * FROM answers WHERE Student=?");
                choice.setString(1, message);
                ResultSet rsChoise = choice.executeQuery();
                while (rsChoise.next()) {
                    String stud = rsChoise.getString("Student");
                    String first = rsChoise.getString("1");
                    String sec = rsChoise.getString("2");
                    String third = rsChoise.getString("3");
                    String four = rsChoise.getString("4");
                    String five = rsChoise.getString("5");
                    String six = rsChoise.getString("6");
                    String seven = rsChoise.getString("7");
                    String eigth = rsChoise.getString("8");
                    String nine = rsChoise.getString("9");
                    String ten = rsChoise.getString("10");
                    String namee = rsChoise.getString("testName");

                    out.write(namee + ": " + first + " " + sec + " " + third + " " + four + " " + five + " " + six + " " + seven + " " + eigth + " " + nine + " " + ten + "\n");
                    out.flush();

                }
            }
        }
    }

    private void uploadLecture(Connection connection, String firstWord) throws IOException {
        if (firstWord.equals("upload")) {
            out.write("Выберите лекцию которую хотите загрузить\n");
            out.flush();

            String path = "C:\\Users\\Bel\\Desktop\\ClientServerWithMysqlAndTxt\\src\\main\\resources\\newLecture\\";

            String list[] = new File(path).list();
            for (int i = 0; i < list.length; i++)
                out.write(list[i] + "\n");
            out.flush();
            if (in.hasNext()) {
                String message = in.nextLine();
                String upPath = "C:\\Users\\Bel\\Desktop\\ClientServerWithMysqlAndTxt\\src\\main\\resources\\Lecture\\" + message;
                InputStream in = new FileInputStream(path + message);
                OutputStream out = new FileOutputStream(upPath);
                int length = 4096;
                byte[] buffer = new byte[length];
                int readed; //возвращает количество записанных байт
                while ((readed = in.read(buffer)) != -1) {
                    out.write(buffer, 0, readed);


                }
            }
        }
    }

    private void makeOtherCommand(Connection connection) throws SQLException, IOException {
        out.write("\nХотите выполнить другую команду?\nЕсли да, то введите 'y'\nЕсли нет, то введите 'n'\n");
        out.flush();
        if (in.hasNext()) {
            String message = in.nextLine();
            if (message.equals("y")) {
                out.flush();
                recordClientMessage();
                choiceOfCommand(connection);
            }
            if (message.equals("n")) {
                out.write("Bye, my dear client");
                connection.close();
            }
        } else connection.close();
    }

    private void choiceOfCommand(Connection connection) throws SQLException, IOException {
        if (words.size() <= 1) {
            testing(connection, words.get(0));
            uploadLecture(connection, words.get(0));
            createTest(connection, words.get(0));
            readLecture(connection, words.get(0));
            checkTest(connection, words.get(0));
            words.clear();
        } else {
            out.write("Такой команды нет\n Попробуйте снова\n\n");
            out.flush();
            recordClientMessage();
            choiceOfCommand(connection);
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?useSSL=false&serverTimezone=UTC",
                "root",
                "root");
    }
}

