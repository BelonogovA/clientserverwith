package ru.belonogov.client;

public class App {

    public static final String SERVER_HOST = "localhost"; //127.0.0.1
    public static final int SERVER_PORT = 8181;

    public static void main(String[] args) {
        new ClientWindow(SERVER_HOST, SERVER_PORT);
    }
}
